###############################################################################

variable "role" {}

###############################################################################

variable "class"               { default = "dns"    }
variable "architecture"        { default = "x86_64" }
variable "root_device_type"    { default = "ebs"    }
variable "virtualisation_type" { default = "hvm"    }

###############################################################################

variable "force"  { default = true        }
variable "region" { default = "eu-west-1" }

###############################################################################

variable "description" { default = "DNS over TLS / DNS over HTTPS server with ad-blocking AMI" }

###############################################################################

locals {
  name = format("%s/%s/%s/%s", var.class, var.architecture, var.virtualisation_type, var.root_device_type)
}

# Temporary workaround for: https://github.com/hashicorp/packer/issues/11011

data "null" "session" {
  input = format("%s-aws", var.class)
}

###############################################################################

build {
  sources = [
    "source.amazon-ebs.object"
  ]

  provisioner "shell" {
    script = format("%s/scripts/init.sh", path.root)
  }

  provisioner "shell" {
    script            = format("%s/scripts/pacman.sh", path.root)
    expect_disconnect = true
  }

  provisioner "shell" {
    script       = format("%s/scripts/init.sh", path.root)
    pause_before = "30s"
  }

  provisioner "shell" {
    script = format("%s/scripts/hosts.sh", path.root)
  }

  provisioner "shell" {
    script = format("%s/scripts/aur.sh", path.root)
  }

  provisioner "shell" {
    script = format("%s/scripts/coredns.sh", path.root)
  }

  provisioner "shell" {
    script = format("%s/scripts/bitservices-letsencrypt.sh", path.root)
  }

  provisioner "shell" {
    script = format("%s/scripts/cleanup.sh", path.root)
  }
}

###############################################################################
