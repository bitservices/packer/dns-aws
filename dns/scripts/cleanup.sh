#!/bin/bash -e
#
# Cleans up AMI images that were launched previously with cloud-init ready for
# re-packaging into a new AMI.
#
###############################################################################

set -o pipefail

###############################################################################

hash rm
hash find
hash sudo
hash xargs
hash pacman

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Removing Orphaned Dependancies...                                    -"
echo "------------------------------------------------------------------------"

if PACMAN_ORPHAN_LIST="$(pacman -Qdtq)"; then
  sudo pacman -Rns --noconfirm ${PACMAN_ORPHAN_LIST}
fi

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Running pacman Clean...                                                 -"
echo "------------------------------------------------------------------------"

echo -e "y\ny" | sudo pacman -Scc

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Cleanup...                                                           -"
echo "------------------------------------------------------------------------"


sudo 2> /dev/null find /var/log       -mindepth 1 -type f -print | sudo 2> /dev/null xargs rm -rfv
sudo 2> /dev/null find /var/lib/cloud -mindepth 1 -print         | sudo 2> /dev/null xargs rm -rfv
sudo 2> /dev/null find /var/cache     -mindepth 1 -print         | sudo 2> /dev/null xargs rm -rfv

if [ -f "${HOME}/.ssh/authorized_keys" ]; then rm -fv "${HOME}/.ssh/authorized_keys"; fi

for HOSTKEY in /etc/ssh/ssh_host_*_key; do sudo rm -fv "${HOSTKEY}"; done

###################################################################################################

echo "------------------------------------------------------------------------"
echo "- DONE!                                                                -"
echo "------------------------------------------------------------------------"

###############################################################################
