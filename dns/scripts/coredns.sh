#!/bin/bash -e
#
# Installs and configures CoreDNS.
#
###############################################################################

set -o pipefail

###############################################################################

hash rm
hash git
hash tee
hash sudo
hash chmod
hash chown
hash pacman
hash makepkg

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Cloning CoreDNS...                                                   -"
echo "------------------------------------------------------------------------"

git clone "https://aur.archlinux.org/coredns-bin.git"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Patching Package...                                                  -"
echo "------------------------------------------------------------------------"

cd coredns-bin

tee "0001-1.12.0.patch" <<PKGPATCH
From b9b58869044ec8b18e6bde6d5aa5a96974f6637e Mon Sep 17 00:00:00 2001
From: Rich <git zero at bitservices dot io>
Date: Mon, 3 Mar 2025 19:52:11 +0000
Subject: [PATCH] 1.12.0

---
 PKGBUILD | 6 +++---
 1 file changed, 3 insertions(+), 3 deletions(-)

diff --git a/PKGBUILD b/PKGBUILD
index c229d9a..f15e5dd 100644
--- a/PKGBUILD
+++ b/PKGBUILD
@@ -5,7 +5,7 @@
 # Contributor: Tristan Hill

 pkgname=coredns-bin
-pkgver=1.11.1
+pkgver=1.12.0
 pkgrel=1
 pkgdesc="A DNS server that chains plugins"
 arch=('x86_64' 'aarch64')
@@ -19,8 +19,8 @@ source_x86_64=(coredns_${pkgver}_x86_64.tar.gz::https://github.com/coredns/cored
 source_aarch64=(coredns_${pkgver}_aarch64.tar.gz::https://github.com/coredns/coredns/releases/download/v${pkgver}/coredns_${pkgver}_linux_arm64.tgz)
 sha256sums=('030cd8e938c293c11a9acdb09b138f98b37874772072336792ec4bf0d9eff9b1'
             'e3cc35967f12c8bca2961f4d98413958649072492fe37052249a8cbcd2313ed1')
-sha256sums_x86_64=('f96cdee0934c5c12a28bb0fb080bed688fdd7bfdeae2f64984f02bdec2d65498')
-sha256sums_aarch64=('725ee697b45fff0c77d63b931977090aa0e93a617a782eac1bc4a996ce0248e2')
+sha256sums_x86_64=('c835bd5e9ba9a98df9631e5a3f0effc60f03ccb9c687c91ae2c85ebf7ab1fc34')
+sha256sums_aarch64=('4946f3110fee46e53acfaef51ebc1a5a844b50411974fd652fa6935128f56271')

 package() {
     install -Dm755 "$srcdir/coredns" "$pkgdir/usr/bin/coredns"
--
2.48.1
PKGPATCH

patch -p1 -i "0001-1.12.0.patch"
rm -v "0001-1.12.0.patch"
cd ..

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Making Package...                                                    -"
echo "------------------------------------------------------------------------"

cd coredns-bin
makepkg --syncdeps --install --noconfirm
cd ..

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Removing Build Files...                                              -"
echo "------------------------------------------------------------------------"

rm coredns-bin -rfv

###############################################################################

echo "------------------------------------------------------------------------"
echo "- Configuring...                                                       -"
echo "------------------------------------------------------------------------"
sudo tee "/etc/coredns/Corefile" <<COREFILE
quic://.:853 tls://.:853 https://.:443 {
  reload
  tls "/coredns-certs/certificate.crt" "/coredns-certs/key.pem"
  timeouts {
    read 30s
    write 60s
    idle 300s
  }
  cache {
    success 12800
    denial 6400
    prefetch 3 5m
    serve_stale
  }
  hosts /etc/hosts.block {
    reload 0s
    fallthrough
  }
  forward . tls://2606:4700:4700::1111 tls://2606:4700:4700::1001 {
    tls
    tls_servername one.one.one.one
    policy random
  }
# forward . tls://2a0f:fc80::ffff tls://2a0f:fc81::ffff {
#   tls
#   tls_servername open.dns0.eu
#   policy random
# }
  prometheus :9153
}
COREFILE

sudo chmod 644 "/etc/coredns/Corefile"

sudo mkdir "/coredns-certs"
sudo chmod 555 "/coredns-certs"

###############################################################################

echo "------------------------------------------------------------------------"
echo "- DONE!                                                                -"
echo "------------------------------------------------------------------------"

###############################################################################
