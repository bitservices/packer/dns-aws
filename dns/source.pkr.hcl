###############################################################################

variable "source_ena"         { default = true         }
variable "source_type"        { default = "t3a.micro"  }
variable "source_sriov"       { default = true         }
variable "source_username"    { default = "archlinux"  }
variable "source_interface"   { default = "public_dns" }
variable "source_t_unlimited" { default = false        }

###############################################################################

variable "source_ami_name"        { default = "archlinux64/linux/*" }
variable "source_ami_owner"       { default = "656332075272"        }
variable "source_ami_most_recent" { default = true                  }

###############################################################################

data "amazon-ami" "object" {
  owners      = [ var.source_ami_owner ]
  region      =   var.region
  most_recent =   var.source_ami_most_recent

  filters = {
    name                = var.source_ami_name
    architecture        = var.architecture
    root-device-type    = var.root_device_type
    virtualization-type = var.virtualisation_type
  }

  assume_role {
    role_arn     = var.role
    session_name = data.null.session.output
  }
}

###############################################################################

source "amazon-ebs" "object" {
  region                = var.region
  ami_name              = local.name
  source_ami            = data.amazon-ami.object.id
  ena_support           = var.source_ena
  ssh_username          = var.source_username
  ssh_interface         = var.source_interface
  instance_type         = var.source_type
  sriov_support         = var.source_sriov
  ami_description       = var.description
  force_deregister      = var.force
  enable_t2_unlimited   = var.source_t_unlimited
  force_delete_snapshot = var.force

  assume_role {
    role_arn     = var.role
    session_name = data.null.session.output
  }
}

###############################################################################
